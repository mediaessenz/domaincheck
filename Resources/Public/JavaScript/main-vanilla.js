const getLabel = function (key) {
    if (typeof TYPO3.lang[key] !== 'undefined') {
        return TYPO3.lang[key];
    } else {
        return '';
    }
};

const checkDomain = function (form, tlds, sld, goButton) {
    const l = Ladda.create(goButton);
    l.start();

    const domaincheckPath = window.domaincheck.path;

    let topLevelDomains = Array.from(tlds.selectedOptions).map(option => parseInt(option.value));

    if (topLevelDomains.length === 0) {
        topLevelDomains = Array.from(tlds.options, option => parseInt(option.value));
    }

    const dataToSend = {
        tlds: topLevelDomains,
        sld: sld.value,
    };

    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(dataToSend),
    };

    fetch(domaincheckPath, options)
    .then((response) => {
        // Stop loading indicator
        l.stop();
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then((data) => {
        let result = '<div class="tx-domaincheck-search-result"><h2>' + getLabel('whois_response_result') + '</h2><div class="table-responsive"><table class="table table-striped">';
        const separator = window.domaincheck.orderPage.indexOf('?') !== -1 ? '&' : '?';

        data.forEach(function (value, index) {
            if (value.status === 'free') {
                result += '<tr><td>' + getLabel('whois_response_available').replace('%', value.domain) + '</td><td class="text-right"><a href="' + window.domaincheck.orderPage + separator + 'domain=' + encodeURIComponent(value.domain) + '" class="btn btn-success">' + getLabel('order_btn') + ' <span class="fas fa-check"></span></a></td></tr>';
            } else {
                result += '<tr><td>' + getLabel('whois_response_occupied').replace('%', value.domain) + '</td><td class="text-right"><a class="btn btn-danger" href="http://www.' + value.domain + '" target="_blank">' + getLabel('occupied_btn') + ' <span class="fas fa-times"></span></a></td></tr>';
            }
        });

        result += '</table></div></div>';

        // Insert the HTML result after the form element
        form.insertAdjacentHTML('afterend', result);
    })
    .catch((error) => {
        // Handle errors
        console.error('Error fetching data:', error);
    });

    // Return false to prevent default form submission behavior
    return false;
};

document.addEventListener('DOMContentLoaded', function () {

    const domainCheckSearchForms = [...document.querySelectorAll('.tx-domaincheck-search-form')];
    if (domainCheckSearchForms.length > 0) {
        domainCheckSearchForms.forEach(function (domainCheckSearchForm) {
            const uid = domainCheckSearchForm.dataset.uid;
            const goButton = document.getElementById('tx-domaincheck-search-go-' + uid);
            document.getElementById('tx-domaincheck-sld-' + uid).addEventListener('keyup', function (event) {
                goButton.disabled = event.target.value.length < 2;
            });

            domainCheckSearchForm.addEventListener('submit', function (event) {
                event.preventDefault();
                if (domainCheckSearchForm.nextSibling.classList && domainCheckSearchForm.nextSibling.classList.contains('tx-domaincheck-search-result')) {
                    domainCheckSearchForm.nextSibling.remove();
                }
                checkDomain(domainCheckSearchForm, document.getElementById('tx-domaincheck-tlds-' + uid), document.getElementById('tx-domaincheck-sld-' + uid), goButton);
            });
        });
    }
});
