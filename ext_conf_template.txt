# cat=basic; type=string; label=Path to whois command: Enter /usr/bin/whois to use command line whois instead of socket connection.
whois_command = /usr/bin/whois

# cat=basic; type=string; label=Use proxy server: Not needed if you use an inwx account
proxy.server =

# cat=basic; type=string; label=Regex if domain is registered: Not needed if you use an inwx account
proxy.regex_registered =

# cat=basic; type=string; label=Regex if domain is available: Not needed if you use an inwx account
proxy.regex_free =

# cat=basic; type=string; label=Regex if domain is invalid: Not needed if you use an inwx account
proxy.regex_invalid =

# cat=basic; type=string; label=INWX Username
inwx_domain_robot_service.username =

# cat=basic; type=string; label=INWX Password
inwx_domain_robot_service.password =

# cat=basic; type=string; label=INWX Shared Secret: Enter code shown below QR-Code during activating "Mobile TAN"
inwx_domain_robot_service.sharedSecret =

# cat=basic; type=string; label=INWX Language
inwx_domain_robot_service.language = de

# cat=basic; type=boolean; label=Enable debugging of domain check middleware
debug =

# cat=basic; type=string; label=inwx domain check middleware url path
inwx_domain_check_middleware_path = /check-domain-status
