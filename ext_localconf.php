<?php

defined('TYPO3') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Domaincheck',
    'Check',
    [
        \MEDIAESSENZ\Domaincheck\Controller\SearchController::class => 'search',
    ]
);

/*
 * ContentElementWizard for Check
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:domaincheck/Configuration/TsConfig/ContentElementWizard.tsconfig">'
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['afterBuildingFinished'][1637165566] = \MEDIAESSENZ\Domaincheck\Hooks\FormElementHooks::class;

if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('form')) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
        module.tx_form {
            settings {
                yamlConfigurations {
                    1637165566 = EXT:domaincheck/Configuration/Form/Setup.yaml
                }
            }
        }
        plugin.tx_form {
            settings {
                yamlConfigurations {
                    1637165566 = EXT:domaincheck/Configuration/Form/Setup.yaml
                }
            }
        }
    '));
}
