.. include:: /Images/Images.rst.txt

.. _introduction:

============
Introduction
============

.. only:: html

	:ref:`what` | :ref:`screenshots` |  :ref:`installation`

.. _what:

What does it do?
================

Domaincheck is an extension to check the availability of domains using the domrobot api of INWX:
https://www.inwx.com/en/offer/api

Alternatively the state can be checked by using a local whois command or a whois proxy.

The search result show the state if a domain is free or occupied. If it is free, it shows a link to an order form.

The extension depends on EXT:typoscript_rendering and EXT:form.


.. _screenshots:

Example Screenshots
===================


Frontend: Search form
---------------------

.. include:: /Images/Frontend1.rst.txt

A form with second level domain to search for and a multi select box for tlds.


Frontend: Search form with entries
----------------------------------

.. include:: /Images/Frontend2.rst.txt

A form with some user input


Frontend: Search result
-----------------------

.. include:: /Images/Frontend3.rst.txt

Result of a domain search with buttons to a form


Backend: Plugin
---------------

.. include:: /Images/Backend1.rst.txt

New content element


Backend: Plugin settings
------------------------

.. include:: /Images/Backend2.rst.txt

Example settings to allow only specific tld in frontend


.. _installation:

Installation
============

Load and install this extension into your TYPO3 installation using extension manager or composer.


Configuration
=============

..  rst-class:: bignums

1. Configure the extension inside extension manager.

   Since this extension is primary designed to work with the api of inwx, you have to create an on
   there side first:
   https://www.inwx.de/en/customer/signup

   Since it is nowadays strongly recommended to use MFA, you should also active this option inside
   their (INWX) backend:

   #.  Click on "Settings > Access data" and select the "Mobile TAN" tab.
   #.  Click on the button "Activate mobile TAN procedure" and copy the 32-character code shown under the
       QR code into your clipboard.
   #.  Open the extension manager inside the TYPO3 backend (Administration > Settings > Extension Configuration)
   #.  Paste the "Shared Secret" from your clipboard into the field "INWX Shared Secret" of the domaincheck settings.
       To use the API you also have to add the username and password of your INWX account inside here.
   #.  Finally you have to finish the configuration on the INWS side. To do so, open your Google Authenticator App
       and add a new Account by scanning the QR-Code and entering the generated code into the corresponding
       field of the INWX ui.

   Alternatively you also can use a local whois command or a whois proxy, which needs further configurations like
   regex definitions to detect free, already registered or invalid domains.

   .. include:: /Images/ExtensionManager.rst.txt

2. Add the static template "Main (domaincheck)" to your page template.

3. Add the plugin "Domaincheck" on a page and configure it as you like.

4. For the order form add a second new page with a form content element where selected the provided
   "Domainregistration" form under Plugin > General > Form definition.
   To set your own email receiver, check "Override finisher settings" and add your data at the upcoming
   "Email to receiver (you)" tab.
   Since EXT:form caches forms by default and there is (currently) no build in way to prevent this behaviour
   (see: https://forge.typo3.org/issues/91419), this extension comes with a middleware to clear pages with the
   cache tag `domain-order-form`, if the url contains the query parameter `?domain=`. So just add this cache tag
   to the form page and you are good to go.

Sponsoring
==========

This extension and the manual took many hours to create.
If this extension helps you in any way to meet your business needs,
please consider to give something back.

Here are two ways to balance energy:

Patreon
-------

Support me on `patreon.com <https://www.patreon.com/alexandergrein>`__

PayPal
------

Support me on `paypal.com <https://www.paypal.me/AlexanderGrein/25>`__.

It's just one click away!

Rate it
-------

In any case, please rate my extension here: https://extensions.typo3.org/extension/domaincheck

.. include:: /Images/TerLike.rst.txt

and if you are using composer to install it, here as well: https://packagist.org/packages/mediaessenz/domaincheck

.. include:: /Images/PackagistLike.rst.txt

..  note::
   To do so, you have to login to the corresponding systems first.

Thank you!
