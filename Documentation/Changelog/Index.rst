.. _changelog:

Changelog
=========

.. t3-field-list-table::
   :header-rows: 1

   -  :Version:   Version
      :Date:      Release Date
      :Changes:   Release Description

   -  :Version:   3.0.0
      :Date:      2025-02-06
      :Changes:   Add TYPO3 13 compatibility; Use Querybuilder instead of extbase repository inside
                  InwxCheckDomainStatusMiddleware.php

   -  :Version:   2.1.0
      :Date:      2024-07-25
      :Changes:   Switch to phpDocumentor;

   -  :Version:   2.0.0
      :Date:      2023-08-04
      :Changes:   Add middleware to clear form page; Remove jQuery dependency; Replace selectize.js by vanilla js;
                  PHP 8 fixes for OTP generator; Use new icon registration; Improve
                  description of how to get INWX shared secret; Add TYPO3 12 compatibility;
                  Remove TYPO3 10 compatibility; Improve documentation;

   -  :Version:   1.3.5
      :Date:      2023-04-09
      :Changes:   Add tag for new v1 branch

   -  :Version:   1.3.4
      :Date:      2022-08-01
      :Changes:   Change project git urls in composer.json; Add gitlab ci config

   -  :Version:   1.3.3
      :Date:      2022-02-03
      :Changes:   Change documentation to fit new registration form

   -  :Version:   1.3.0
      :Date:      2021-11-17
      :Changes:   Code refactoring for TYPO3 11; Add registration form based on EXT:form

   -  :Version:   1.2.2
      :Date:      2021-03-17
      :Changes:   Make TYPO3 10/11 compatible; set compatibility range; move tsconfig to separate
                  folder and other small cleanups;

   -  :Version:   1.1.0
      :Date:      2017-04-25
      :Changes:   Code refactoring for TYPO3 8.7

   -  :Version:   1.0.1
      :Date:      2017-02-21
      :Changes:   Change outdated jquery ajax success to done

   -  :Version:   1.0.0
      :Date:      2016-07-31
      :Changes:   Initial upload to TER


All changes are documented on `https://gitlab.com/mediaessenz/domaincheck <https://gitlab.com/mediaessenz/domaincheck>`_
