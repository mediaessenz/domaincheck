<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

return [
    'extension-domaincheck' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:domaincheck/Resources/Public/Icons/Extension.svg'
    ],
];
