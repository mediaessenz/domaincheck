<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois',
        'label' => 'server',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'server',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'rootLevel' => 1,
        'searchFields' => 'server',
        'iconfile' => 'EXT:domaincheck/Resources/Public/Icons/Whois.svg',
    ],
    'types' => [
        [
            'showitem' => 'server,idn,regex_registered,regex_free,regex_invalid',
        ],
    ],
    'columns' => [
        'server' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois.server',
            'config' => [
                'type' => 'input',
            ],
        ],
        'idn' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois.idn',
            'config' => [
                'type' => 'input',
                'eval' => 'int',
                'size' => 8,
            ],
        ],
        'regex_registered' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois.regex_registered',
            'config' => [
                'type' => 'input',
            ],
        ],
        'regex_free' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois.regex_free',
            'config' => [
                'type' => 'input',
            ],
        ],
        'regex_invalid' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_whois.regex_invalid',
            'config' => [
                'type' => 'input',
            ],
        ],
    ],
];
