<?php
defined('TYPO3') or die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Domaincheck',
    'Check',
    'Domaincheck'
);

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['domaincheck_check'] = 'select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['domaincheck_check'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('domaincheck_check', 'FILE:EXT:domaincheck/Configuration/FlexForms/Check.xml');
