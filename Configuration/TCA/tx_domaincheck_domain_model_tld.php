<?php

return [
    'ctrl' => [
        'title' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_tld',
        'label' => 'tld',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'default_sortby' => 'tld',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'rootLevel' => 1,
        'searchFields' => 'tld',
        'iconfile' => 'EXT:domaincheck/Resources/Public/Icons/Tld.svg',
    ],
    'types' => [
        [
            'showitem' => 'tld,regex_check,whois',
        ],
    ],
    'columns' => [
        'tld' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_tld.tld',
            'config' =>
                [
                    'type' => 'input',
                ],
        ],
        'regex_check' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_tld.regex_check',
            'config' =>
                [
                    'type' => 'input',
                ],
        ],
        'whois' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:domaincheck/Resources/Private/Language/locallang.xlf:tx_domaincheck_domain_model_tld.whois',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_domaincheck_domain_model_whois',
                'foreign_table_where' => 'ORDER BY tx_domaincheck_domain_model_whois.uid',
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    ],
];
