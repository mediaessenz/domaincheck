<?php

return [
    'frontend' => [
        'mediaessenz/domaincheck/inwx-domain-check-status' => [
            'target' => \MEDIAESSENZ\Domaincheck\Middleware\InwxCheckDomainStatusMiddleware::class,
            'before' => [
                'typo3/cms-frontend/site',
            ],
        ],
        'mediaessenz/domaincheck/domain-registration-forms-cache-flush' => [
            'target' => \MEDIAESSENZ\Domaincheck\Middleware\DomainRegistrationFormsCacheFlushMiddleware::class,
            'before' => [
                'typo3/cms-frontend/site',
            ],
        ],
    ],
];
