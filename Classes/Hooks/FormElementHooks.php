<?php
namespace MEDIAESSENZ\Domaincheck\Hooks;

use TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface;

class FormElementHooks
{
    /**
     * @param RenderableInterface $renderable
     * @return void
     */
    public function afterBuildingFinished(RenderableInterface $renderable): void
    {
        if (method_exists($renderable, 'getRootForm') && method_exists($renderable->getRootForm(), 'getIdentifier')) {
            $rootFormIdentifier = $renderable->getRootForm()->getIdentifier();
            if (($domain = $_GET['domain'] ?? false) && str_contains($rootFormIdentifier, 'ext-domaincheck-registration')) {
                $domain = preg_replace("/[\/:*?\"<>\\|]/", "", $domain);
                $identifier = $renderable->getIdentifier();
                if ($identifier === 'domainname') {
                    $renderable->setLabel($domain);
                }
                if ($identifier === 'domain') {
                    $renderable->setDefaultValue($domain);
                }
            }
        }
    }
}
