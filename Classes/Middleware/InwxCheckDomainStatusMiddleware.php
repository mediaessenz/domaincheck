<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Domaincheck\Middleware;

use JsonException;
use MEDIAESSENZ\Domaincheck\Exception\CallFailedException;
use MEDIAESSENZ\Domaincheck\Service\InwxDomRobotService;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\TypoScript\FrontendTypoScript;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class InwxCheckDomainStatusMiddleware implements MiddlewareInterface
{
    protected $extensionConfiguration;
    protected bool $debug = false;
    protected string $urlPath = '/check-domain-status';

    public function __construct(
        private ResponseFactoryInterface $responseFactory,
        private InwxDomRobotService $inwxDomainRobotService,
        private ConnectionPool $connectionPool
    )
    {
        $this->extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        try {
            $this->urlPath = $this->extensionConfiguration->get('domaincheck', 'inwx_domain_check_middleware_path');
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException) {
            $this->urlPath = '/check-domain-status';
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getMethod() !== 'POST' || !$this->isMatchingRequest($request)) {
            return $handler->handle($request);
        }

        try {
            $this->debug = (bool)$this->extensionConfiguration->get('domaincheck', 'debug');
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException) {
            $this->debug = false;
        }
        try {
            $username = $this->extensionConfiguration->get('domaincheck', 'inwx_domain_robot_service/username');
            $password = $this->extensionConfiguration->get('domaincheck', 'inwx_domain_robot_service/password');
            $sharedSecret = $this->extensionConfiguration->get('domaincheck', 'inwx_domain_robot_service/sharedSecret');
            $language = $this->extensionConfiguration->get('domaincheck', 'inwx_domain_robot_service/language');
        } catch (ExtensionConfigurationExtensionNotConfiguredException|ExtensionConfigurationPathDoesNotExistException $exception) {
            $response = $this->responseFactory->createResponse();
            $response->withStatus(500, $this->debug ? $exception->getMessage() : 'Set debug to see error messages');
            return $response;
        }

        $params = json_decode($request->getBody()->getContents(), true);

        $topLevelDomains = $this->getTopLevelDomains($params['tlds']);

        $secondLevelDomain = strtolower(trim($params['sld'] ?? ''));

        // remove www. if set
        if (str_starts_with($secondLevelDomain, 'www.')) {
            $secondLevelDomain = substr($secondLevelDomain, 4);
        }

        // remove .tld from sld if set
        if ($pos = strpos($secondLevelDomain, '.')) {
            $secondLevelDomain = substr($secondLevelDomain, 0, $pos);
        }

        $whoisResponse = [];

        if ($username && $password && $topLevelDomains && $secondLevelDomain) {
            // If INWX username and password is set try to connect to INWX
            try {
                $inwxDomainRobotServiceConnection = $this->inwxDomainRobotService
                    ->setLanguage($language)
                    ->useLive()
                    ->useJson()
                    ->login($username, $password, $sharedSecret);

            } catch (CallFailedException|JsonException $callFailedException) {
                $response = $this->responseFactory->createResponse();
                $response->withStatus(500, $this->debug ? $callFailedException->getMessage() : 'Set debug to see error messages');
                return $response;
            }

            if ((int)($inwxDomainRobotServiceConnection['code'] ?? 0) === 1000) {
                foreach ($topLevelDomains as $tld) {
                    /** @var Tld $tld */
                    $domain = strtolower($secondLevelDomain . '.' . $tld['tld']);
                    try {
                        $result = $this->inwxDomainRobotService->call('domain', 'check',  ['domain' => $domain]);
                        $whoisResponse[] = [
                            'domain' => $domain,
                            'status' => $result['resData']['domain'][0]['status']
                        ];
                    } catch (CallFailedException|JsonException $callFailedException) {
                        $response = $this->responseFactory->createResponse();
                        $response->withStatus(500, $this->debug ? $callFailedException->getMessage(): 'Set debug to see error messages');
                        return $response;
                    }
                }
            }
            try {
                $this->inwxDomainRobotService->logout();
            } catch (CallFailedException|JsonException $exception) {
                $response = $this->responseFactory->createResponse();
                $response->withStatus(500, $this->debug ? $exception->getMessage(): 'Set debug to see error messages');
                return $response;
            }
        }

        $response = $this->responseFactory->createResponse()
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withAddedHeader('Pragma', 'no-cache')
            ->withAddedHeader('Expire', '0');
        $response->getBody()->write(json_encode($whoisResponse));

        return $response;
    }

    /**
     * @param array $topLevelDomains
     * @return array
     */
    public function getTopLevelDomains(array $topLevelDomains = []): array
    {
        $table = 'tx_domaincheck_domain_model_tld';
        $queryBuilder = $this->connectionPool->getQueryBuilderForTable($table);
        $queryBuilder->getRestrictions()
            ->removeAll()
            ->add(GeneralUtility::makeInstance(DeletedRestriction::class));

        if ($topLevelDomains) {
            return $queryBuilder
                ->select('tld')
                ->from($table)
                ->where(
                    $queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter($topLevelDomains, Connection::PARAM_INT_ARRAY))
                )
                ->executeQuery()
                ->fetchAllAssociative();
        }

        return $queryBuilder
            ->select('tld')
            ->from($table)
            ->executeQuery()
            ->fetchAllAssociative();
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    protected function isMatchingRequest(ServerRequestInterface $request): bool
    {
        return $request->getRequestTarget() === $this->urlPath;
    }
}
