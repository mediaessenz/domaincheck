<?php

namespace MEDIAESSENZ\Domaincheck\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class Tld
 * @package MEDIAESSENZ\Domaincheck\Domain\Model
 */
class Tld extends AbstractEntity
{
    /**
     * @var string
     */
    protected $tld;

    /**
     * @var string
     */
    protected $regexCheck;

    /**
     * @var \MEDIAESSENZ\Domaincheck\Domain\Model\Whois
     */
    protected $whois;

    /**
     * @return string
     */
    public function getTld()
    {
        return $this->tld;
    }

    /**
     * @param string $tld
     * @return Tld
     */
    public function setTld($tld)
    {
        $this->tld = $tld;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegexCheck()
    {
        return $this->regexCheck;
    }

    /**
     * @param string $regexCheck
     * @return Tld
     */
    public function setRegexCheck($regexCheck)
    {
        $this->regexCheck = $regexCheck;
        return $this;
    }

    /**
     * @return Whois
     */
    public function getWhois()
    {
        return $this->whois;
    }

    /**
     * @param \MEDIAESSENZ\Domaincheck\Domain\Model\Whois $whois
     * @return Tld
     */
    public function setWhois($whois)
    {
        $this->whois = $whois;
        return $this;
    }
}
