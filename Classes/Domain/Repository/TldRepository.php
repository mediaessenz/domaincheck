<?php

namespace MEDIAESSENZ\Domaincheck\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

class TldRepository extends Repository
{
    /**
     * @param array $uids
     * @return array|QueryResultInterface
     * @throws InvalidQueryException
     */
    public function findByUids(array $uids): QueryResultInterface|array
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching($query->in('uid', $uids));

        return $query->execute();
    }
}
