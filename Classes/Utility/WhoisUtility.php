<?php

namespace MEDIAESSENZ\Domaincheck\Utility;

use MEDIAESSENZ\Domaincheck\Domain\Model\Tld;

class WhoisUtility
{

    /**
     * @param string $sld
     * @param array $tlds
     * @param string $whoisCommand
     * @param string $whoisProxy
     *
     * @return array
     */
    public static function checkDomains($sld, $tlds, $whoisCommand, $whoisProxy)
    {

        foreach ($tlds as $tld) {
            /** @var Tld $tld */
            $domain = strtolower($sld . '.' . $tld->getTld());
            $whoisResponse[] = [
                'domain' => $domain,
                'status' => CheckUtility::checkResponse(
                    shell_exec($whoisCommand . ' -h ' . $whoisProxy['server'] . ' ' . idn_to_ascii($domain)),
                    $whoisProxy,
                    ['###DOMAIN###' => $domain]
                )
            ];
        }

        return $whoisResponse;
    }

}
