<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Domaincheck\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Information\Typo3Version;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SearchController extends AbstractController
{
    public function searchAction(): ResponseInterface
    {
        $versionInformation = GeneralUtility::makeInstance(Typo3Version::class);
        if ($versionInformation->getMajorVersion() <= 12) {
            $this->view->assign('data', $this->configurationManager->getContentObject()->data);
        }
        $this->view->assign('tlds', $this->getTlds($this->settings['tlds']));
        $this->view->assign('inwxDomainCheckMiddlewarePath', $this->extConf['inwx_domain_check_middleware_path'] ?? '/check-domain-status');

        return $this->htmlResponse();
    }
}
