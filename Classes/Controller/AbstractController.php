<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Domaincheck\Controller;

use MEDIAESSENZ\Domaincheck\Domain\Repository\TldRepository;
use MEDIAESSENZ\Domaincheck\Service\InwxDomRobotService;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

class AbstractController extends ActionController
{
    /**
     * @var array
     */
    protected $extConf = [];

    /**
     * @var TldRepository
     */
    protected TldRepository $tldRepository;

    /**
     * @var InwxDomRobotService
     */
    protected InwxDomRobotService $inwxDomainRobotService;

    protected bool $debug = false;

    /**
     * @throws ExtensionConfigurationPathDoesNotExistException
     * @throws ExtensionConfigurationExtensionNotConfiguredException
     */
    public function __construct(InwxDomRobotService $inwxDomainRobotService, TldRepository $tldRepository, ExtensionConfiguration $extensionConfiguration)
    {
        $this->inwxDomainRobotService = $inwxDomainRobotService;
        $this->tldRepository = $tldRepository;
        $this->extConf = $extensionConfiguration->get('domaincheck');
        $this->debug = (bool)($this->settings['debug'] ?? false);
    }

    /**
     * @param string $tlds
     * @return array|QueryResultInterface
     */
    public function getTlds(string $tlds = '')
    {
        if ($tlds) {
            return $this->tldRepository->findByUids(GeneralUtility::trimExplode(',', $tlds, true));
        }

        return $this->tldRepository->findAll();
    }

}
